import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { VideoChatComponent } from './../components/video-chat/video-chat-page/video-chat.component';
import { WaitModalComponent } from './../components/video-chat/video-chat-page/wait-modal.component';
import { VideoChatTestComponent } from './../components/video-chat/video-chat-test/video-chat-test.component';
import { VideoChatSettingsComponent } from './../components/video-chat/video-chat-settings/video-chat-settings.component';

import { AuthService } from './../services/auth.service';
import { CallService } from './../services/call.service';
import { ClientService } from './../services/client.service';
import { ChatService } from './../services/chat.service';
import { SocketService } from './../services/socket.service';
import { VideoService } from './../services/video.service';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';


import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import {Http, HttpModule} from "@angular/http";

@NgModule({
  declarations: [
    AppComponent,
    VideoChatComponent,
    VideoChatTestComponent,
    VideoChatSettingsComponent,
    WaitModalComponent
  ],
  exports: [
  	VideoChatComponent,
  	VideoChatTestComponent,
  	VideoChatSettingsComponent
  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    MatInputModule,
    MatGridListModule,
    MatDialogModule,
    MatDividerModule,
    MatTabsModule,
    MatListModule,
    MatIconModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule
  ],
  providers: [
  	AuthService,
  	CallService,
  	ClientService,
  	ChatService,
  	SocketService,
  	VideoService
  ],
  bootstrap: [AppComponent],
  entryComponents: [WaitModalComponent]
})
export class AppModule { }
