// manages video elements

import {Injectable} from "@angular/core";

declare var navigator: any;


@Injectable()
export class VideoService {
  localStream: any;
  public facing = 'front';
  isFirstTimeCallConnect = true;

  constructor() {
    navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
  }


  connect(audio, video, facing, videoSource) {
    let self = this;
    return new Promise((resolve, reject) => {
      let connect = () => {


        let mediaConfig: any;
        mediaConfig = {
          audio: true,
          video: true
        };

        console.log(mediaConfig);
        navigator.getUserMedia(mediaConfig,
          (stream) => {
            console.log('got local MediaStream: ', stream, stream.getTracks());
            this.localStream = stream;
            resolve(stream);
          },
          (error) => {
            console.error('getUserMedia failed: ', error);
            reject();
          }
        );

        if (this.isFirstTimeCallConnect) {
          this.isFirstTimeCallConnect = false;

        }

      };

      if (this.localStream) {
        self.disconnect().then(connect);
      } else {
        connect();
      }
    });
  }


  connectForFirstCall(audio, video, facing, videoSource) {
    let self = this;
    return new Promise((resolve, reject) => {
      let connect = () => {
        let mediaConfig: any;
        mediaConfig = {
          audio: true,
          video: video
        };

        navigator.getUserMedia(mediaConfig,
          (stream) => {
            console.log('got local MediaStream: ', stream, stream.getTracks());
            this.localStream = stream;
            resolve(stream);
          },
          (error) => {
            console.error('getUserMedia failed: ', error);
            reject();
          }
        );

        if (this.isFirstTimeCallConnect) {
          this.isFirstTimeCallConnect = false;

        }
      };
      if (this.localStream) {
        self.disconnect().then(connect);
      } else {
        connect();
      }
    });
  }

  // get a list of devices
  devices() {
    return new Promise((resolve, reject) => {
      navigator.mediaDevices.enumerateDevices().then(devices => {
        resolve(devices);
      });
    });
  }

  // disconnect the media stream
  disconnect() {
    return new Promise((resolve, reject) => {
      if (this.localStream) {
        var tracks = this.localStream.getTracks();
        for (var x in tracks) {
          tracks[x].stop();
        }
        console.log('stoping stream', this.localStream.getTracks());
        this.localStream = null;
      }
      resolve();
    });
  }
}
