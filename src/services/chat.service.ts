import {Injectable, NgZone} from '@angular/core';
import {SocketService} from './socket.service';
import {AuthService} from './auth.service';
import {USER_CHAT_TOKEN} from '../environments/environment';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class ChatService {
  user = null;
  chats = [];
  lastMessage: string;
  userId = '';
  room: any;
  public onMessageReceived: BehaviorSubject<any>;
  // manages chats
  constructor(private zone: NgZone,
  			  private auth: AuthService,
              private socketService: SocketService) {
    this.onMessageReceived = new BehaviorSubject('');

    this.userId = this.auth.getId();


    // broadcast an event when there is a chat mesasage recieved
    // another way to do this would be to forward the message directly
    this.socketService.on('message', (message) => {
      console.log('SOCKET chat-message');

      let sender = '';
      if (this.lastMessage === message) {
        sender = this.userId;
      } else {
        this.onMessageReceived.next(message.message);
      }
    });


    this.socketService.on('room', room => {
      console.log('GET ROOM-ID', room);
      this.room = room;
      localStorage.setItem('room-id', room);
    });


    this.socketService.on('chats', chats => {
      zone.run(() => {

        // merge new chats to the chat array but dont replace the array itself
        for (let chat of chats) {
          let i = true;
          for (let c of this.chats) {
            if (c.id === chat.id) {
              i = false;
              break;
            }
          }
          if (i) {
            this.chats.push(chat);
          }
        }

        for (let chat of this.chats) {
          chat.lastMarked = this.parseMessage(chat.lastMessage);
        }

      });
    });
  }


  public send(message) {
    console.log('send message:-> ', message);


    if (!this.room) {
      this.room = localStorage.getItem('room-id');
      console.log('SAVING ROOM_ID FOR CHAT SERVICE!', this.room);

    }
    // @todo: make a promise so we know when it has fully been sent

    console.log('room to send:' + this.room);

    let toSend = {
      message: {
        body: message,
        from: 'client',
        to: 'agent'
      },
      room: this.room
    };

    this.socketService.emit(false, 'message',
      toSend
    );
    this.onMessageReceived.next({
      body: message,
      from: 'client',
      to: 'agent'
    });
  }

  private parseMessage(message) {
    if (!message) {
      return null;
    }
  }
}
