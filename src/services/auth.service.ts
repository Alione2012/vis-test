import {Injectable} from '@angular/core';
import {USER_CHAT_TOKEN} from '../environments/environment';

@Injectable()
export class AuthService {
  private userId: string = null;
  constructor() {
  }
  public getId():string {
  	if(this.userId) return this.userId;

  	let tokenItem = localStorage.getItem(USER_CHAT_TOKEN);
    if (tokenItem) {
      this.userId = tokenItem;
    } else {
      this.userId = this.makeId(10);
      localStorage.setItem('userChatToken', this.userId);
    }

    return this.userId;
  }

  private makeId(len): string {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$';
    for (let i = 0; i < (len || 10); i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }
}
