import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {Documentation, REQUEST_ID_FOR_CALL, SERVER} from '../environments/environment';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {AuthService} from './auth.service';

//import {CustomerService} from 'app/saveup-common/customer/shared/customer.service';
//import {CustomerModel} from '../../customer/shared/customer.model';
//import {UserModel} from '../../common-shared/user/user.model';

@Injectable()
export class ClientService {
  newUser: {};

  constructor(private http: Http, public auth: AuthService) {
  }

  public requestPost(documentation: Documentation): Observable<any> {
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    let authInfo = JSON.parse(sessionStorage.getItem('auth'));
    console.log('authinfo', authInfo);
    let options = new RequestOptions({headers: headers});
    let body = {
      userId: this.auth.getId(),
      documentation
    };
    console.log("body:", body);
    let url = SERVER + '/api/v1/' + 'client/requests';
    return this.http.post(url, JSON.stringify(body), options)
      .map(res => res.json());
  }

  public checkStatus(): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let requestId = JSON.parse(localStorage.getItem(REQUEST_ID_FOR_CALL));
    let options = new RequestOptions({headers: headers});
    let url = SERVER + 'client/requests/' + requestId;


    return this.http.get(url, options)
      .map(res => res.json());
  }

  public sendPostRequestForRequestId() {
    let user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    let content: any[] = [];
    let address = user.address;

    let info = user.info;
    if (address.addressLine1) {
      content.push({key: 'addressLine1', value: address.addressLine1});
    }

    if (address.addressLine2) {
      content.push({key: 'addressLine2', value: address.addressLine2});
    }

    if (address.city) {
      content.push({key: 'city', value: address.city});
    }

    if (address.country) {
      content.push({key: 'country', value: address.country});
    }

    if (address.state) {
      content.push({key: 'state', value: address.state});
    }

    if (address.zipCode) {
      content.push({key: 'zipCode', value: address.zipCode});
    }

    if (address._id) {
      content.push({key: '_id', value: address._id});
    }


    if (info.birthday) {
      content.push({key: 'birthday', value: info.birthday});
    }

    if (info.birthdayPlace) {
      content.push({key: 'birthdayPlace', value: info.birthdayPlace});
    }

    if (info.email) {
      content.push({key: 'email', value: info.email});
    }

    if (info.familyName) {
      content.push({key: 'familyName', value: info.familyName});
    }

    if (info.firstName) {
      content.push({key: 'firstName', value: info.firstName});
    }

    if (info.gender) {
      content.push({key: 'gender', value: info.gender});
    }

    if (info.phone) {
      content.push({key: 'phone', value: info.phone});
    }

    if (info.type) {
      content.push({key: 'type', value: info.type});
    }

    if (info.iban) {
      content.push({key: 'iban', value: info.iban});
    }

    if (info.accountName) {
      content.push({key: 'accountName', value: info.accountName});
    }

    if (info.policyId) {
      content.push({key: 'policyId', value: info.policyId});
    }

    if (info.contractLanguage) {
      content.push({key: 'contractLanguage', value: info.contractLanguage});
    }

    if (info.passportNnumber) {
      content.push({key: 'passport number', value: info.passportNnumber});
    }

    let documentation = new Documentation();
    documentation.content = content;
    console.log(content);

    /**
     * send request to create room
     * return - requestId of the room
     */
    console.log('requestPost send -2');
    return this.requestPost(documentation);
  }


  public checkPermissions() {
    return new Promise((resolve) => {
      this.sendPostRequestForRequestId().subscribe((res) => {
        resolve(true);
      });
    });

  }

  public checkPermissionsAndShowAlert() {
    return new Promise((resolve) => {

    });
  }

  createUser(array: any) {
    //this.newUser = this.customerService.getCustomerModel();

    array = array.filter(obj => {console.log(obj.key, obj.key !== "isComplete"); return obj.key !== "isComplete";});

    console.log('arrayFromUser', array);
    console.log('newUser before clear', this.newUser);

    let clearFlattenData = (object, prefix?) => {

      prefix = (prefix) ? prefix + '/' : '';

      for (let key in object) {
        if (typeof object[key] === 'object') {
          clearFlattenData(object[key], prefix + key);
        } else {
          if (key === "isComplete") {
            continue;
          }
          let item = this.getValueFromArray(array, prefix + key);
          object[key] = item;
        }
      }
    };

    clearFlattenData(this.newUser);

    //this.newUser['confirmedVideoID'] = true;
    //console.log(this.newUser);
    //this.customerService.saveCustomer(this.newUser);
  }

  getValueFromArray(array, searchValue) {
    let isFind = false;
    let value = '';
    array.forEach(item => {
      if (!isFind) {
        if (item.key.toUpperCase() === searchValue.toUpperCase()) {
          isFind = true;
          value = item.value;
        }
      }
    });
    return value;
  }


}
