// handles incoming and outgoing video calls

import {ApplicationRef, Injectable} from "@angular/core";
import {VideoService} from "./video.service";
import {DomSanitizer} from "@angular/platform-browser";
import {SocketService} from "./socket.service";
import {REQUEST_ID_FOR_CALL, ICE_SERVERS} from "../environments/environment";
import {ClientService} from "./client.service";
import {BehaviorSubject} from "rxjs";


declare var
  cordova: any,
  window: any,
  RTCSessionDescription: any,
  RTCPeerConnection: any,
  RTCIceCandidate: any;
declare var $: any;


@Injectable()
export class CallService {
  idIncement = 0;
  maxTimer = 2000000;
  facing = 'front';
  pickupTimeout = null;
  contactId = null;
  isInCall = false;
  isCalling = false;
  isAnswering = false;
  muted = false;
  lastState = null;
  localStream = null;
  peerConnection = null;
  remoteVideo = null;
  localVideo = null;
  peerConnectionConfig = null;
  modalShowing = false;
  modal = null;
  isCallInitiator = false;
  public videoInputsId: string [];
  public audioInputsId: string [];
  public isCallAlreadyEnd = false;
  public isLocalVideoCreated = false;

  private sendReq = 'signal';
  private reseiveReq = 'signal';

  private requestId: string;
  private userId: string;
  private endCallTimeout: any = null;
  private isAgentDisconnected = false;
  private opponentRequstId: string;
  private responseIceCount = 0;
  private isCallSuccessfulConnected = false;
  private statusFromAgent: any;
  private currentCameraIndex = 1;


  public onAgentConnected: BehaviorSubject<any>;
  public onAgentSetStatus: BehaviorSubject<any>;
  public onGoAway: BehaviorSubject<any>;
  public onAddStream: BehaviorSubject<any>;
  public onCallError: BehaviorSubject<any>;


  public sendAuthentificateSocket() {
    let requestId = JSON.parse(localStorage.getItem(REQUEST_ID_FOR_CALL));
    console.log('send auth!:', requestId, this.userId);
    console.debug('send auth!:', requestId, this.userId);
    this.socket.emit(false, 'authenticate',
      {
        requestId: requestId,
        role: 'client',
        userId: this.userId
      });
  }


  constructor(public ref: ApplicationRef,
              private sanitizer: DomSanitizer,
              public socket: SocketService,
              public video: VideoService,
              private clientService: ClientService) {
    this.onAgentConnected = new BehaviorSubject('init');
    this.onAgentSetStatus = new BehaviorSubject('init');
    this.onGoAway = new BehaviorSubject('init');
    this.onAddStream = new BehaviorSubject('init');
    this.onCallError = new BehaviorSubject('init');

    this.userId = this.clientService.auth.getId();

    this.socket.on('connection', (message) => {
      console.log('CALLBACK connection');
    });

    this.socket.on('agent-disconnected', () => {
      console.log('agent-disconnected', true);
      this.isAgentDisconnected = true;
    });

    this.socket.on('agent-connected', (message) => {
      console.log('agent-connected');
      this.onAgentConnected.next('yes');
    });

    //neew to init contactId
    this.contactId = 'phone';
    // browser compatability for web views

    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
    window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;

    // STUN/TURN ice servers for connection negotiation
    this.peerConnectionConfig = {
      'iceServers': ICE_SERVERS
    };


    navigator.mediaDevices.enumerateDevices().then(info => {
      this.gotDevices(info);
    });

    this.socket.on(this.reseiveReq, (data) => {
      this.requestId = JSON.parse(localStorage.getItem(REQUEST_ID_FOR_CALL));
      console.log('Message: ', data);

      if (data.contact && !this.opponentRequstId) {
        this.opponentRequstId = data.contact;
      }

      console.log('this.opponentRequstId', this.requestId, this.opponentRequstId);

      if (true) {
        console.log('Message222: ', data);
        console.log('this.isCallInitiator: ', this.isCallInitiator);

        try {
          switch (data.type) {
            case 'statusWasSet':
              console.log('statusSet: ', data.status.status);
              this.statusFromAgent = data.status.status;
              if (data.updatedDocumantation) {
                //this.clientService.createUser(data.updatedDocumantation);
              }
              if (this.isCallAlreadyEnd) {
                this.onAgentSetStatus.next(this.statusFromAgent);
                this.statusFromAgent = null;
              } else {
                this.end('');
              }

              break;

            case 'ping':
              console.log('ping');
              if (this.endCallTimeout) {
                clearTimeout(this.endCallTimeout);
              }
              this.endTimeout();
              break;

            case 'call1':
              if (true) {
                console.log('case :call');
                console.log('incoming call...', data);
                clearTimeout(this.pickupTimeout);
                this.pickupTimeout = null;
                this.isCalling = false;
                this.isAnswering = true;
                this.answer();
              }

              break;

            case 'answer1':
              if (true) {
                console.log('case :answer');
                clearTimeout(this.pickupTimeout);
                this.pickupTimeout = null;
                this.isCalling = false;
                this.refreshVideos(10);
                this.call(true, this.contactId).then((res) => {
                  console.log();
                });
              }

              break;

            case 'ignore':
            case 'cancel':
              console.log('case :ignor/cancel');

              this.end();
              break;

            case 'end1':
              console.log('case :end');
              if (this.isInCall || this.isAnswering) {
                this.end();
              }
              break;
          }
        } catch (e) {
          console.error(e);
        }


        if (data.sdp) {
          this.peerConnection.setRemoteDescription(new RTCSessionDescription(data.sdp), () => {
            if (data.sdp.type === 'offer') {
              this.peerConnection.createAnswer(d => {
                this.gotDescription(d);
              }, e => {
                console.log('error creating answer', e);
              });
            }
          }, e => {console.log('error', e);});
        } else if (data.ice) {
          this.peerConnection.addIceCandidate(new RTCIceCandidate(data.ice));
        }
      }


      if (data.sdp) {
        this.peerConnection.setRemoteDescription(new RTCSessionDescription(data.sdp), () => {
          if (data.sdp.type === 'offer') {
            this.peerConnection.createAnswer(d => {
//
              this.gotDescription(d);
            }, e => {
              console.log('error creating answer', e);
            });
          }
        }, e => {console.log('error', e);});
      } else if (data.ice) {
        this.peerConnection.addIceCandidate(new RTCIceCandidate(data.ice));
      }
    });
  }

  clearEndCAllTimeout() {
    if (this.endCallTimeout) {
      clearTimeout(this.endCallTimeout);
      this.endCallTimeout = null;
    }
  }


  endTimeout() {
    this.endCallTimeout = setTimeout(() => {
      if (!this.statusFromAgent) {
        console.log('end from endTimeout', this.isInCall);
        this.end('endTimeOut');
      }
    }, 10000);
  }

  gotDevices(deviceInfos) {
    // Handles being called several times to update labels. Preserve values.
    this.videoInputsId = [];
    this.audioInputsId = [];
    console.log('getting video inputs: ');

    for (var i = 0; i !== deviceInfos.length; ++i) {
      var deviceInfo = deviceInfos[i];
      var option = document.createElement('option');
      option.value = deviceInfo.deviceId;
      if (deviceInfo.kind === 'audioinput') {
        console.log(deviceInfo);
      } else if (deviceInfo.kind === 'audiooutput') {
        this.audioInputsId.push(deviceInfo.deviceId);

      } else if (deviceInfo.kind === 'videoinput') {
        console.log(deviceInfo);
        this.videoInputsId.push(deviceInfo.deviceId);
      } else {
        console.log('Some other kind of source/device: ', deviceInfo);
      }
    }
  }

  // place a new call
  public triggerCall(contact): any {
    this.requestId = JSON.parse(localStorage.getItem(REQUEST_ID_FOR_CALL));
    this.showModal();
    if (this.isInCall) {
      return;
    }
    this.isCalling = true;
    this.isCallInitiator = true;
    this.isCallAlreadyEnd = false;
    this.socket.emit(true, this.sendReq,
      {
        contact: this.requestId,
        type: 'call1'
      }
    );

    return new Promise(() => {
      console.log('');
    });
  }

  public refreshVideos(times) {
    // tell the modal that we need to revresh the video

    this.ref.tick();
  }

  // open the call modal
  showModal() {
    this.modalShowing = true;
  }


  doChangeCamera() {
    this.peerConnection.removeStream(this.peerConnection.getLocalStreams()[0]);

    let doCreateVideo: any;
    doCreateVideo = this.createVideo(this.videoInputsId[this.currentCameraIndex]).then((stream) => {
      setTimeout(() => {
        this.addStream(stream, 100);
        this.peerConnection.addStream(stream);
        this.createDescr();
      }, 100);
    });

    if (this.currentCameraIndex === 0) {
      let videoInputsId = this.videoInputsId[this.currentCameraIndex];
      console.log(videoInputsId);
      if (videoInputsId) {
        doCreateVideo;
      }
      this.currentCameraIndex = 1;
    } else {
      let videoInputsId = this.videoInputsId[this.currentCameraIndex];
      console.log(videoInputsId);
      if (videoInputsId) {
        doCreateVideo;
      }
      this.currentCameraIndex = 0;
    }
  }


  public end(status?: any) {
    if (!this.isCallAlreadyEnd) {
      console.log('end method executed!');
      this.isCallAlreadyEnd = true;
      this.isCallInitiator = false;
      if (this.peerConnection) {
        this.peerConnection.close();
      }
      this.localVideo = null;
      this.remoteVideo = null;
      this.isAnswering = false;
      this.isCalling = false;
      this.isInCall = false;
      this.localStream = null;
      this.video.facing = 'front';

      this.video.disconnect().then(() => {
        this.refreshVideos(10);
      });

      if (!this.contactId) {
        return;
      }

      this.socket.emit(true, this.sendReq, {
        type: 'end1'
      });
      this.responseIceCount = 0;

      if (this.isAgentDisconnected) {
        this.isAgentDisconnected = false;
        this.onGoAway.next('agent-disconnected');

      } else {
        this.onGoAway.next(status);
      }

      if (this.statusFromAgent) {
        setTimeout(() => {
          console.log('send agent-set-status');
          this.onAgentSetStatus.next(this.statusFromAgent);
          this.statusFromAgent = null;
        }, 200);
      }
    }
  }


  private gotDescription(description) {
    console.log('got description', description, this.contactId);
    this.peerConnection.setLocalDescription(description, () => {
      this.socket.emit(true, this.sendReq,
        {
          contact: this.contactId,
          sdp: description
        }
      );
    }, e => {
      console.log('set description error', e);
    });
  }

  private gotIceCandidate(event) {
    if (event.candidate !== null) {
      this.socket.emit(true, this.sendReq, {
        'ice': event.candidate
      });
    }
  }


  private gotRemoteStream(event) {
    this.isCallSuccessfulConnected = true;
    console.log('got remote stream');
    this.remoteVideo = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(event.stream));
  }


  // add local stream


  private addStream(stream, timeout) {
    this.localStream = stream;
    // Create a video element in memory (not yet in the DOM).
    setTimeout(() => {
      console.log('addStream()', stream);
      this.onAddStream.next(stream);
    }, timeout);
  }

  // preview local video as full screen

  private onNegotiationNeeded() {
    console.log('negotation nedded');
  }

  // begin a call using webrtc

  private call(isInitiator, contactId) {
    return new Promise((res, err) => {
      console.log('calling ' + contactId + ', isInitiator: ' + isInitiator);
      var connect = () => {
        this.peerConnection = new RTCPeerConnection(this.peerConnectionConfig);
        console.log('CREATING peerConnection');
        console.log(this.peerConnection);


        this.peerConnection.onicecandidate = this.gotIceCandidate.bind(this);
        this.peerConnection.onaddstream = this.gotRemoteStream.bind(this);
        this.peerConnection.onnegotiationneeded = this.onNegotiationNeeded.bind(this);
        this.peerConnection.oniceconnectionstatechange = event => {

          this.lastState = event.target.iceConnectionState;
          console.log('ice state', this.lastState, this.responseIceCount);
          this.responseIceCount = this.responseIceCount + 1;
          this.responseIceCount = this.responseIceCount + 1;
          if (this.lastState === 'disconnected' || this.lastState === 'closed') {
            console.log('ice state', this.lastState);
            this.isInCall = false;
            this.peerConnection = null;

            if (this.isCallSuccessfulConnected) {
              this.end('');
            } else {
              this.end('disconnected');
              this.onCallError.next('call-error');
            }
          } else if (this.lastState === 'completed ') {
            this.isInCall = true;
          }
        };
        this.peerConnection.addStream(this.localStream);

        if (isInitiator) {
          console.log('creating offer');
          this.peerConnection.createOffer(d => {
            this.gotDescription(d);
          }, e => {
            console.log('error creating offer', e);
          });
        } else {
          //
        }
      };

      if (!this.localStream) {
        this.video.connect(true, true, this.facing, this.videoInputsId[0]).then(stream => {
          this.addStream(stream, 1000);
          connect();
        });
      } else {
        connect();
      }
      res(this.peerConnection);
    });
  }

  private createDescr() {
    this.peerConnection.createOffer()
      .then((offer) => {
        this.peerConnection.setLocalDescription(offer);
      })
      .then(() => {
        this.socket.emit(true, this.sendReq, {
            sdp: this.peerConnection.localDescription
          }
        );
      });
  }

  private createVideo(videoSource?: any) {
    return new Promise((res, reject) => {
      if (this.localStream) {
        this.localStream.getTracks().forEach(function (track) {
          console.log(track);
          track.stop();
        });
      }

      let mediaConfig: any;
      mediaConfig = {
        audio: true,
        video: {deviceId: videoSource ? {exact: videoSource} : undefined},
      };

      navigator.mediaDevices.getUserMedia(mediaConfig)
        .then(stream => {
          console.log('Stream:');
          console.log(stream);
          this.localStream = stream;
          res(stream);
        })
        .catch(e => {
          console.error(e);
        });
    });
  }


  // cancel a call being placed
  private cancel() {
    this.socket.emit(true, this.sendReq, {
      type: 'cancel'
    });
    this.end();
  }

  // ignore an incomming call
  private ignore(end, name) {
    this.socket.emit(true, this.sendReq, {
      type: 'ignore'
    });
    if (!end) return;
    this.end();
  }

  // answer in incoming call
  private answer() {
    if (this.isInCall) {
      return;
    }

    clearTimeout(this.pickupTimeout);
    this.pickupTimeout = null;

    this.isInCall = true;
    this.isAnswering = false;
    this.call(false, this.contactId).then((res) => {
      //
    });

    console.log('call callback from answer;');
    setTimeout(() => {
      this.socket.emit(true, this.sendReq,
        {
          contact: this.requestId,
          type: 'answer1'
        }
      );
    });
    this.refreshVideos(10);
  }
}
