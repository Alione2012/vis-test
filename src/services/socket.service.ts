// handle socket io connections

import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {REQUEST_ID_FOR_CALL, SOCKET_SERVER} from "../environments/environment";


@Injectable()
export class SocketService {
    socket = null;
    public roomId = '';

    constructor() {
        // connect to our server
        // change this url here
        this.socket = io.connect(SOCKET_SERVER);

    }

    // generate a unique custom request id
    // send an event and get a response back
    public promise(eventName, request) {
        return new Promise((resolve, reject) => {

            let success = response => {
                console.debug(eventName + '|' + request.responseName + ': complete!');
                this.socket.removeListener(request.responseName, success);
                resolve(response);
            };

            request.responseName = '$response$' + this.makeId(10) + '$';
            console.debug(eventName + '|' + request.responseName + ': Sending socket promise...');
            this.socket.on(request.responseName, success);
            this.socket.emit(eventName, request);
        });
    }


    public closeSocket() {
        if (this.socket) {
            console.log(this.socket);
            this.socket.disconnect();
            console.log('after', this.socket);
        }
    }

    public emit(isWebrtcRequest: boolean, ...args: any[]) {
        if (isWebrtcRequest) {
            if (this.roomId === '') {
                console.log('set room id: ', JSON.parse(localStorage.getItem(REQUEST_ID_FOR_CALL)));
                this.roomId = JSON.parse(localStorage.getItem(REQUEST_ID_FOR_CALL));
            }
            let data: any = args[1];
            data.room = this.roomId;
            args[1] = data;
        }
        console.log('room send: ');
        console.log(this.roomId)
        // console.log('send socket log:', args);
        this.socket.emit(...args);
    }

    public on(name, data) {
        // console.log('simple socket', name, data);
        this.socket.on(name, data);
    }

    public clearRoomId() {
        this.roomId = '';
    }

    private makeId(len) {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$';
        for (var i = 0; i < (len || 10); i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
}
