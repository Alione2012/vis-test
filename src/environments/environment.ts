// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false
};

export const USER_CHAT_TOKEN: string = 'userChatToken';
export const REQUEST_ID_FOR_CALL: string = 'requestIdForCall';
export let SERVER = localStorage.getItem('SERVER');
export let SOCKET_SERVER = localStorage.getItem('SERVER');

/*
if (environment.tenant === 'msv') {
  SERVER = 'https://vis.savvisave.com';
  SOCKET_SERVER = 'https://vis.savvisave.com';
} */

// enables or disables chat sounds. usefull for development

// STUN/TURN ice servers for connection negotiation
export const ICE_SERVERS = [
    {
        urls: 'stun:stun.l.google.com:19302'
    },
    {
        urls: 'stun:stun.services.mozilla.com'
    },
    {
        urls: 'stun:numb.viagenie.ca',
        username: 'ewgeniy@gmail.com',
        credential: 'aramis_2708'
    },
    {
        urls: 'turn:numb.viagenie.ca',
        username: 'ewgeniy@gmail.com',
        credential: 'aramis_2708'
    }
];


export interface ChatMessage {
    body: string,
    from: string,
    to: string
}

export class Content {
    key: string;
    value: string;
}

export class Documentation {
    content: any[];
}


