import {Component, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'video-chat-settings',
  templateUrl: './video-chat-settings.html',
  styleUrls: ['./video-chat-settings.css']

})
export class VideoChatSettingsComponent{
  @Output() next: EventEmitter<any> = new EventEmitter();
  public server: string = "vis.dev.aurocraft.com";
	constructor() {
  }

  public start() {
  	localStorage.setItem('SERVER', 'https://' + this.server.replace(/(^\w+:|^)\/\//, ''));
  	this.next.emit();
  }

}
