import {Component} from '@angular/core';

@Component({
  selector: 'video-chat-test',
  templateUrl: './video-chat-test.html'
  //styleUrls: ['./video-chat-test.css']

})
export class VideoChatTestComponent{
	public isCall: boolean = false;
	constructor() {
  }

  public startCall(server) {
  	this.isCall = true;
  }

}
