/**
 * Created by Marc on 01.12.2016.
 */
import {AfterViewInit, ApplicationRef, Component, ElementRef, OnDestroy, OnInit, ViewChild, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {Subscription} from 'rxjs';
import {CallService} from '../../../services/call.service';
import {ChatService} from '../../../services/chat.service';
import {SocketService} from '../../../services/socket.service';
import {ChatMessage, Documentation, REQUEST_ID_FOR_CALL, USER_CHAT_TOKEN, SOCKET_SERVER} from '../../../environments/environment';
import {WaitModalComponent} from './wait-modal.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import {ClientService} from '../../../services/client.service';
//import {CustomerService} from '../../customer/shared/customer.service';

@Component({
  selector: 'video-chat',
  templateUrl: './video-chat.html',
  styleUrls: ['./video-chat.css']

})
export class VideoChatComponent implements OnDestroy, OnInit, AfterViewInit, OnChanges {
  @Input() isCall: boolean;
  @Output() isCallChange: EventEmitter<any> = new EventEmitter();
  @ViewChild('localVideo') localVideo: ElementRef;
  @ViewChild('remoteVideo') remoteVideo: ElementRef;
  @ViewChild('chatContent') chatCont: ElementRef;

  agentConnectedSubscription: Subscription;
  agentSetStatusSubscription: Subscription;
  goAwaySubscription: Subscription;
  addStreamSubscription: Subscription;
  callErrorSubscription: Subscription;
  messageReceivedSubscription: Subscription;
  chatMessages = [];
  userChatToken: string;
  chatLocalStream: any;
  messageHandleWrap = null;
  isWaitStatusOpen: boolean;
  server = SOCKET_SERVER;
  hiddenVideo: boolean = true;

  data = {
    message: null,
  };

  // modals
  private waitAgentModal: any;
  private waitStatusModal: any;

  constructor(private ref: ApplicationRef,
              public callService: CallService,
              private clientService: ClientService,
              private chatService: ChatService,
              private socketSerivce: SocketService,
              public  dialog: MatDialog) {
  }

  ngOnChanges(changes: SimpleChanges) {
  	if(changes["isCall"].currentValue === true) {
  		this.startCall();
  	}
  }

  startCall() {
    let documentation = new Documentation();
    this.chatMessages = [];

  	this.clientService.requestPost(documentation).subscribe(
      resp => {
        localStorage.setItem(REQUEST_ID_FOR_CALL, JSON.stringify(resp.requestId));
        this.initVideoChat();
        this.openWaitModal();
        this.userChatToken = localStorage.getItem(USER_CHAT_TOKEN);
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit(): void {
    /**
     * fire when localStream added in callService.addStream()
     * @type {Subscription}
     */

    this.addStreamSubscription = this.callService.onAddStream.subscribe(stream => {
      if (stream !== 'init') {
        this.chatLocalStream = stream;
        console.log('ADDED LOCAL STREAM:');
        console.log(stream);
        this.localVideo.nativeElement.srcObject = this.chatLocalStream;
        this.localVideo.nativeElement.muted = true;
        //this.ref.tick();
      }
    });

    /**
     * fire when agent press 'accept call' button and we can start the call. Method this.callService.triggerCall() start call
     * @type {Subscription}
     */
    this.agentConnectedSubscription = this.callService.onAgentConnected.subscribe(message => {
      if (message !== 'init') {
        console.log('agentConnected', message);
        this.waitAgentModal.close();
        this.showVideo();
        this.callService.triggerCall('').then(() => {

        });
      }
    });

    /**
     * '' - should wait when agent set call status
     * 'agent-disconnected' - fire when agent disconnected from call (some error - close browser or browser window with call);
     * 'disconnected' - fire when some call error
     * @type {Subscription}
     */
    this.goAwaySubscription = this.callService.onGoAway.subscribe(status => {
      console.log('go away', status);
      if (status !== 'init') {
        if (status === '') {
          this.openWaitStatusModal();

        } else if (status === 'agent-disconnected') {
          console.log('this.goAwaySubscription', 'agent-disconnected');

          let disconnectedModal = this.dialog.open(WaitModalComponent, {
            data: 'disconnected'
          });

          disconnectedModal.afterClosed().subscribe(result => {
            this.goToStartPage();
          });

        } else if (status === 'disconnected') {
          console.log('this.goAwaySubscription', 'disconnected');
          if (!this.isWaitStatusOpen) {
            this.openWaitStatusModal();
          }
        }
      }
    });

    /**
     * fire when agent set call status. Should display status to client and then redirect
     * @type {Subscription}
     */
    this.agentSetStatusSubscription = this.callService.onAgentSetStatus.subscribe(status => {
      if (status !== 'init') {
        console.log('agentSetStatusSubscription', status);

        this.hideVideo();
        this.waitStatusModal.close();
        this.openModalWithStatus(status)
      }

    });

    /**
     *
     * return to start call page
     * @type {Subscription}
     */
    this.callErrorSubscription = this.callService.onCallError.subscribe(error => {
      if (error !== 'init') {
        console.log('error', error);

        let dialogRef = this.dialog.open(WaitModalComponent, {
          data: 'disconnected'
        });

        dialogRef.afterClosed().subscribe(result => {
          this.goToStartPage();
        });
      }
    });

    /**
     * fire when new chat message received from agent
     * @type {Subscription}
     */
    this.messageReceivedSubscription = this.chatService.onMessageReceived.subscribe(message => {
      if (message !== 'init') {
        console.log('message', message);
        this.handleMessage(message);
      }
    });
  }


  ngAfterViewInit(): void {

  }

  /**
   * need to unsubscribe all subscriptions and stop stream tracks
   */
  ngOnDestroy(): void {

    this.dialog.closeAll();

    this.callService.onCallError.next('init');
    this.callService.onAddStream.next('init');
    this.callService.onAgentSetStatus.next('init');
    this.callService.onGoAway.next('init');
    this.callService.onAgentConnected.next('init');

    if (this.agentConnectedSubscription) {
      this.agentConnectedSubscription.unsubscribe();
    }

    if (this.agentSetStatusSubscription) {
      this.agentSetStatusSubscription.unsubscribe();
    }


    if (this.goAwaySubscription) {
      this.goAwaySubscription.unsubscribe();
    }

    if (this.addStreamSubscription) {
      this.addStreamSubscription.unsubscribe();
    }

    if (this.callErrorSubscription) {
      this.callErrorSubscription.unsubscribe();
    }
    if (this.messageReceivedSubscription) {
      this.messageReceivedSubscription.unsubscribe();
    }

    this.stopChatLocalStream();
    console.log('destroy socket');
    this.socketSerivce.closeSocket();
    localStorage.removeItem('room-id');
    this.socketSerivce.clearRoomId();
  }

  /**
   * if client click endCall button, should execute this method and display modal/alert with question 'End call?' and with 2 button - 'yes' or ''no
   * If click no - hide modal, if yes - should execute follow code:
   * this.callService.end();
   * this.socketSerivce.emit(true, 'signal', {type: 'user-disconnect'});
   * and then wait for agent set call status
   */
  endCall() {

    let dialogRef = this.dialog.open(WaitModalComponent, {
      height: '150px',
      width: '300px',
      data: 'close'
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result === true) {
        this.callService.end();
        this.socketSerivce.emit(true, 'signal', {type: 'user-disconnect'});
        this.openWaitStatusModal();
      }
    });

  }

  /**
   * reconnect to socket and send authentificate request by socket
   */
  initVideoChat() {
    setTimeout(() => {
      console.log('before reconnecting socket...', this.socketSerivce.socket.connected);
      if (!this.socketSerivce.socket.connected) {
        console.log('reconnecting socket...', this.socketSerivce.socket);
        this.socketSerivce.socket.connect();
      }

      setTimeout(() => {
        console.log('reconnect socket', this.socketSerivce.socket);
        this.callService.sendAuthentificateSocket();
      }, 200);
    }, 250);
  }

  /**
   * send message to agent. need to dataBind to input/textArea via [(ngModel)]="data.message"
   */
  send() {
    if (this.data.message && this.data.message !== '') {

      this.chatService.send(this.data.message);
      this.data.message = '';

      setTimeout(() => {
        //scroll chat bottom
        this.scroll();
      }, 100);
    }
  }

  public reloadVideo() {
  	this.localVideo.nativeElement.play();
  	this.remoteVideo.nativeElement.play();
  	//this.ref.tick();
  }

  private getDocumentation(): any {
    /*let user = this.customerService.getCustomerModel();
    let documentation = new Documentation();
    let content: any[] = [];

    let flatten = (object, prefix?) => {

      prefix = (prefix) ? prefix + '/' : '';

      for (let key in object) {
        if (typeof object[key] === 'object') {
          flatten(object[key], prefix + key);
        } else {
          content.push({key: prefix + key, value: object[key]});
        }
      }
    };

    flatten(user);
    documentation.content = content;

    return documentation; */
  }


  /**
   * stopping localStream tracks
   */
  private stopChatLocalStream() {
    if (this.chatLocalStream) {
      let tracks = this.chatLocalStream.getTracks();
      for (var x in tracks) {
        tracks[x].stop();
      }
      console.log('stoping stream', this.chatLocalStream.getTracks());
      this.chatLocalStream = null;
    }
  }


  /**
   * Handle message from agent and push it to array of messages
   * @param data
   */
  private handleMessage(data: any) {
    this.chatMessages.push(data);
    this.scroll();
    console.log(this.chatMessages);
  }

  /**
   * scroll messages content bottom
   */
  private scroll() {
    setTimeout(() => {
      if (this.chatCont) {
        this.chatCont.nativeElement.scrollTop = this.chatCont.nativeElement.scrollHeight;
      }
    }, 100);
  }


  //Toogle video

  private hideVideo() {
    this.hiddenVideo = true;
  }

  private showVideo() {
    this.hiddenVideo = false;
  }


  //Modals

  private openWaitStatusModal() {

    this.hideVideo();

    this.waitStatusModal = this.dialog.open(WaitModalComponent, {
      height: '160px',
      width: '400px',
      disableClose: true,
      data: 'wait-status'
    });

    this.isWaitStatusOpen = true;

  }

  private openWaitModal() {

    this.waitAgentModal = this.dialog.open(WaitModalComponent, {
      height: '220px',
      width: '400px',
      data: 'wait-in-queue'
    });

    WaitModalComponent.cancel.subscribe(() => {
        this.goToStartPage();
        this.waitAgentModal.close();
    });

  }

  private openModalWithStatus(status: string) {

    let statusModal = this.dialog.open(WaitModalComponent, {
      height: '200px',
      width: '240px',
      data: status
    });

    statusModal.afterClosed().subscribe(result => {
      this.goToStartPage();
    });
  }

  private goToStartPage() {
  		this.socketSerivce.clearRoomId();
        this.socketSerivce.closeSocket();
    	this.isCall = false;
    	this.isCallChange.emit();
  }
}
