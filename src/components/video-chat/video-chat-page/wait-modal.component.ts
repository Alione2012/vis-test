import {Component, OnDestroy, Inject, EventEmitter} from "@angular/core";
import {SocketService} from '../../../services/socket.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'wait-modal',
    templateUrl: 'wait-modal.html'
})
export class WaitModalComponent implements OnDestroy {
    public title: string;
    public status: string;
    public isShowButton: boolean;
    public isCloseVideo: boolean;
    public isShowCancelButton: boolean;
    public isWait: boolean;
    public static cancel: EventEmitter<any> = new EventEmitter();
    //user: UserModel;
    //isFromContactDocument = false;

    constructor(
        public dialogRef: MatDialogRef<WaitModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {

        switch(data) {
            case 'close': {
                this.title = 'Are you sure?';
                this.isCloseVideo = true;
                break;
            }

            case 'disconnected': {
                this.title = 'Something went wrong...';
                this.isShowButton = true;
                break;
            }

            case 'wait-in-queue': {
                this.title = 'You are in the queue! Please wait...';
                this.isShowCancelButton = true;
                this.isWait = true;
                break;
            }

            case 'wait-status': {
                this.title = 'Wait for agent set status...';
                this.isWait = true;
                break;
            }

            default: {
                this.title = 'Agent set status:';
                this.status = data;
                this.isShowButton = true;
                break;
            }
        }
    }

    public cancel(){
    	WaitModalComponent.cancel.emit();
    }

    ngOnDestroy(): void {
        //this.unsubscribeAll();
    }
}
