import { VisTestPage } from './app.po';

describe('vis-test App', () => {
  let page: VisTestPage;

  beforeEach(() => {
    page = new VisTestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
